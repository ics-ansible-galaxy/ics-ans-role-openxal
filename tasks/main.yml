---
- name: install required dependencies
  yum:
    name: git
    state: present

- name: install OpenXAL distribution
  unarchive:
    src: "{{ openxal_archive }}"
    dest: /opt
    remote_src: true
    owner: root
    group: root
    creates: "{{ openxal_library }}"

- name: install jelog html
  unarchive:
    src: "{{ openxal_jelog_html }}"
    dest: "{{ openxal_home }}/lib"
    remote_src: true
    owner: root
    group: root
  tags:
    - openxal-jelog

- name: create OpenXAL apps directory
  file:
    path: "{{ openxal_apps_dir }}"
    state: directory
    owner: root
    group: root
    mode: 0755

- name: show app versions
  debug:
    msg: "{{ item }}: {{ lookup('vars', 'openxal_app_' + item + '_version', default=openxal_apps_version) }}"
  loop: "{{ openxal_apps }}"
  tags: versions

- name: install OpenXAL apps
  get_url:
    url: "{{ openxal_base_url }}/openxal.app.{{ item }}/{{ lookup('vars', 'openxal_app_' + item + '_version', default=openxal_apps_version) }}/openxal.app.{{ item }}-{{ lookup('vars', 'openxal_app_' + item + '_version', default=openxal_apps_version) }}.jar"  # noqa 204
    dest: "{{ openxal_apps_dir }}/{{ item }}.jar"
    force: true
    owner: root
    group: root
  loop: "{{ openxal_apps }}"
  tags:
    - openxal-apps

- name: install OpenXAL lattice
  git:
    repo: "{{ openxal_lattice_repo }}"
    dest: "{{ openxal_home }}/optics"
    version: "{{ openxal_lattice_version }}"
  tags:
    - openxal-lattice

- name: create the /opt/OpenXAL link
  file:
    src: "{{ openxal_home }}"
    dest: /opt/OpenXAL
    state: link
    owner: root
    group: root

- name: create the /etc/openxal config directory
  file:
    path: /etc/openxal
    state: directory
    owner: root
    group: root
    mode: 0755

- name: create default OpenXAL preferences
  template:
    src: "{{ item }}.j2"
    dest: "/etc/openxal/{{ item }}"
    owner: root
    group: root
    mode: 0644
  loop:
    - xal.smf.data.prefs
    - xal.extension.application.rbac.prefs
    - xal.plugin.jca.prefs
    - xal.extension.logbook.prefs
    - xal.plugin.olog.prefs
    - xal.extension.fxapplication.prefs

- name: remove old OpenXAL desktop launcher
  file:
    path: "/usr/share/applications/{{ item }}.desktop"
    state: absent
  loop:
    - OpenXAL-Model-Manager
    - OpenXAL-Virtual-Accelerator
    - OpenXAL-Optics-Editor
    - OpenXAL-Optics-Switcher
    - OpenXAL-PV-Correlator
    - OpenXAL-PV-Histogram
    - OpenXAL-PV-Logger
    - OpenXAL-Scan-1D
    - OpenXAL-Scan-2D

- name: remove old OpenXAL desktop menu and lattices directory
  file:
    path: "{{ item }}"
    state: absent
  loop:
    - /etc/xdg/menus/applications-merged/openxal.menu
    - /usr/share/desktop-directories/openxal.directory
    - /opt/lattices

- name: create openxal-launcher.sh script
  template:
    src: openxal-launcher.sh.j2
    dest: /usr/local/bin/openxal-launcher.sh
    owner: root
    group: root
    mode: 0755
  tags:
    - openxal-launcher-script

- name: make sure /usr/local/share/icons directory exist
  file:
    path: /usr/local/share/icons
    state: directory
    owner: root
    group: root
    mode: 0755

- name: copy openxal-icon.png
  copy:
    src: openxal-icon.png
    dest: /usr/local/share/icons/openxal-icon.png
    owner: root
    group: root
    mode: 0644

- name: create OpenXAL desktop launcher
  copy:
    src: OpenXAL-Launcher.desktop
    dest: /usr/share/applications/OpenXAL-Launcher.desktop
    owner: root
    group: root
    mode: 0644

- name: check current installations
  find:
    paths: /opt
    patterns: 'openxal-*'
    file_type: directory
  register: find_openxal
  tags: clean-openxal

- name: remove previous installations
  file:
    path: "{{ item.path }}"
    state: absent
  loop: "{{ (find_openxal.files | sort(attribute='ctime', reverse=True))[openxal_installations_to_keep:] }}"
  when:
    - item.path != openxal_home
  tags: clean-openxal
