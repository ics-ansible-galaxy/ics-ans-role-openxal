import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")

OPENXAL_VERSION = "6.3"


def test_openxal_link(host):
    openxal_link = host.file("/opt/OpenXAL")
    assert openxal_link.is_symlink
    assert openxal_link.linked_to == "/opt/openxal-{}".format(OPENXAL_VERSION)


def test_openxal_installed(host):
    assert host.file("/opt/openxal-{0}/lib/library.jar".format(OPENXAL_VERSION)).exists
    assert host.file(
        "/opt/openxal-{0}/apps/launcher.jar".format(OPENXAL_VERSION)
    ).exists


def test_openxal_preferences(host):
    prefs = host.file("/etc/openxal/xal.smf.data.prefs")
    assert prefs.content_string.strip() == "mainPath=/opt/openxal-{0}/optics/current/main.xal".format(
        OPENXAL_VERSION
    )
    assert host.file("/etc/openxal/xal.plugin.jca.prefs").contains("jca.use_env=true")


def test_openxal_lattice_installed(host):
    assert host.file("/opt/OpenXAL/optics/current/main.xal").exists


def test_openxal_libs(host):
    assert host.file("/opt/OpenXAL/lib/libhdf5_java.so").exists


def test_openxal_jelog_html_installed(host):
    assert host.file(
        "/opt/openxal-{0}/lib/html/elog.css".format(OPENXAL_VERSION)
    ).exists


def test_previous_install_removed(host):
    if (
        host.ansible.get_variables()["inventory_hostname"]
        != "ics-ans-role-openxal-app-versions"
    ):
        # by default 2 versions are kept:
        # - the installed one
        # - the previous one (1.1.0 because 1.0.0 and 1.2.0 were created before in prepare.yml)
        assert host.file("/opt/openxal-1.1.0").exists
        assert host.file("/opt/openxal-{0}".format(OPENXAL_VERSION)).exists
        assert not host.file("/opt/openxal-1.0.0").exists
        assert not host.file("/opt/openxal-1.2.0").exists
    else:
        # openxal_installations_to_keep set to 1
        assert host.file("/opt/openxal-{0}".format(OPENXAL_VERSION)).exists
        assert not host.file("/opt/openxal-1.0.0").exists
        assert not host.file("/opt/openxal-1.1.0").exists
        assert not host.file("/opt/openxal-1.2.0").exists
