# ics-ans-role-openxal

Ansible role to install OpenXAL applications (>= 3.x).

This role is not compatible with OpenXAL 1.x nor OpenXAL 2.x (based on java 8).
It is made to install the apps that were splitted in several repositories in OpenXAL 2.x.
It also depends on java 11.

## Role Variables

```yaml
# openxal.dist version
openxal_version: 5.1
# default version for openxal apps
openxal_apps_version: 5.1
openxal_app_lebt_version: 2.0
openxal_app_modelbrowser_version: 1.0
openxal_app_scanner_version: 8
openxal_app_lattice_editor_version: 1.0
openxal_app_trajectorycorrection_version: 1-b1
openxal_app_trajectorydisplay_version: 4.1
openxal_app_shift_start_version: 1.0
openxal_base_url: "https://artifactory.esss.lu.se/artifactory/OpenXAL/release/org/xal"
openxal_archive: "{{ openxal_base_url }}/openxal.dist/{{ openxal_version }}/openxal.dist-{{ openxal_version }}-dist.tar.gz"
openxal_jelog_version: 2.5
openxal_jelog_html: https://artifactory.esss.lu.se/artifactory/libs-release-local/eu/ess/jelog/{{ openxal_jelog_version }}/html-{{ openxal_jelog_version }}.tar.gz
# total number of openxal installations to keep
# 2 allows to keep the current running version + the one just installed
openxal_installations_to_keep: 2
openxal_use_rbac: "false"
openxal_lattice_repo: https://gitlab.esss.lu.se/ess-crs/openxal-lattice.git
openxal_lattice_version: 1.9
# default lattice path set in the preferences
openxal_lattice_main_path: "{{ openxal_home }}/optics/current/main.xal"
# if "true", use EPICS_CA_ADDR_LIST from the environment variables
# set to "false" to use JCALibrary.properties
openxal_jca_use_env: "true"

```

By default all apps use the `openxal_apps_version` version.
To install an app specific version, define the variable `openxal_app_<name>_version`.

You could for example define `openxal_app_scan1d_version: 2.0`. This applies to all installed apps:

```yaml
openxal_apps:
  - configurator
  - knobs
  - launcher
  - lebt
  - modelbrowser
  - pvhistogram
  - scan1d
  - scan2d
  - scanner
  - trajectorycorrection
  - trajectorydisplay
  - virtualaccelerator
```


## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-openxal
```

## License

BSD 2-clause
